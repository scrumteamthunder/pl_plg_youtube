package com.k12.services.bb.youtube.model;

public class StudentDataModel {
	private String name;
	private String percentComplete;
	private int score;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPercentComplete() {
		return percentComplete;
	}

	public void setPercentComplete(String percentComplete) {
		this.percentComplete = percentComplete;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {

		if (score > 100) {
			score = 100;
		}

		this.score = score;
	}

}
