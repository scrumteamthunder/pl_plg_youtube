package com.k12.services.bb.youtube.model;

public class Account {

	private String userID;
    private String batchUID; 
	private String first;
	private String last;
	private String courseID;

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}
    
    public String getBatchUID() { 
        return batchUID; 
    }
    
    public void setBatchUID(String batchUID) {
        this.batchUID = batchUID; 
    }

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getLast() {
		return last;
	}

	public void setLast(String last) {
		this.last = last;
	}

	public String getCourseID() {
		return courseID;
	}

	public void setCourseID(String courseID) {
		this.courseID = courseID;
	}
}
