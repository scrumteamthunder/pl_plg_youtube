package com.k12.services.bb.youtube.model;

import java.util.List;

import com.k12.services.bb.youtube.model.YouTubeMedia;

public class YouTubeVideo {
 
     private List<String> thumbnails;
     private List<YouTubeMedia> medias;
     private String webPlayerUrl;
     private String embeddedWebPlayerUrl;
     private String videoTitle; 
     private String videoDescription; 
     private String videoHash; 
     
     public List<String> getThumbnails() {
          return thumbnails;
     }
     public void setThumbnails(List<String> thumbnails) {
          this.thumbnails = thumbnails;
     }

     public List<YouTubeMedia> getMedias() {
          return medias;
     }
     public void setMedias(List<YouTubeMedia> medias) {
         this.medias = medias;
     }
     
     public void setVideoHash(String videoHash) { 
    	 this.videoHash = videoHash; 
     }
     
     public String getVideoHash() { 
    	 return videoHash; 
     }
 
     public void setVideoTitle(String videoTitle) { 
    	 this.videoTitle = videoTitle; 
     }
     
     public String getVideoTitle() { 
    	 return videoTitle; 
     }
     
     public void setVideoDescription(String videoDescription) { 
    	 this.videoDescription = videoDescription; 
    	 if (this.videoDescription.length() >= 160) {
    		 this.videoDescription = videoDescription.substring(0, 160).concat("..."); 
     }
 } 
     public String getVideoDescription() { 
    	 return videoDescription; 
     }
     
     public String getWebPlayerUrl() {
          return webPlayerUrl;
     }
     public void setWebPlayerUrl(String webPlayerUrl) {
            this.webPlayerUrl = webPlayerUrl;
     }

     public String getEmbeddedWebPlayerUrl() {
          return embeddedWebPlayerUrl;
     }
     public void setEmbeddedWebPlayerUrl(String embeddedWebPlayerUrl) {
          this.embeddedWebPlayerUrl = embeddedWebPlayerUrl;
     }
 
     public String retrieveHttpLocation() {
          if (medias==null || medias.isEmpty()) {
               return null;
          }
          for (YouTubeMedia media : medias) {
               String location = media.getLocation();
               if (location.startsWith("http")) {
                    return location;
               }
          }
          return null;
      }

}
