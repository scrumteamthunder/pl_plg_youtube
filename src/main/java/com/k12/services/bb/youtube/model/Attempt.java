package com.k12.services.bb.youtube.model;

public class Attempt {

	private String attemptID;
	private String userID;
	private String contentID;
	private String videoID;
	private String courseID;
	private String name;
	private int duration;
	private Float elapsedTime;
	private String role;

	public String getAttemptID() {
		return attemptID;
	}

	public void setAttemptID(String attemptID) {
		this.attemptID = attemptID;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getContentID() {
		return contentID;
	}

	public void setContentID(String contentID) {
		this.contentID = contentID;
	}

	public String getVideoID() {
		return videoID;
	}

	public void setVideoID(String videoID) {
		this.videoID = videoID;
	}

	public String getCourseID() {
		return courseID;
	}

	public void setCourseID(String courseID) {
		this.courseID = courseID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public Float getElapsedTime() {
		return elapsedTime;
	}

	public void setElapsedTime(Float elapsedTime) {
		this.elapsedTime = elapsedTime;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
