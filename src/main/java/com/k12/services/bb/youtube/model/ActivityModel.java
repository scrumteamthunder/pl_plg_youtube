package com.k12.services.bb.youtube.model;

import java.io.Serializable;
import java.util.Map;

public class ActivityModel implements Serializable {
	static final long serialVersionUID = 1;

	Map<String, String> tableData;

	String previewURL = "";
	String aplusContentId = "";
	String createGradeColumn = "";
	int max_point_value; 

	public Map<String, String> gettableData() {
		return tableData;
	}

	public void settableData(Map<String, String> tableData) {
		this.tableData = tableData;
	}

	public String getpreviewURL() {
		return previewURL;
	}

	public void setpreviewURL(String previewURL) {
		this.previewURL = previewURL;
	}

	public String getaplusContentId() {
		return aplusContentId;
	}

	public void setaplusContentId(String aplusContentId) {
		this.aplusContentId = aplusContentId;
	}

	public void setCreateGradeColumn(String createGradeColumn) {
		this.createGradeColumn = createGradeColumn;
	}

	public String getCreateGradeColumn() {
		return createGradeColumn;
	}
	
	public void setMaxPointValue(int max_point_value) { 
		this.max_point_value = max_point_value; 
	}
	
	public int getMaxPointValue() { 
		return max_point_value; 
	}
}