package com.k12.services.bb.plugin.youtube;


import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;

import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.Channel;
import com.google.api.services.youtube.model.ChannelBrandingSettings;
import com.google.api.services.youtube.model.ChannelListResponse;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SearchService implements Serializable {
	static final long serialVersionUID = 1; 
	private static final Logger logger = LoggerFactory.getLogger(SearchService.class);
	
	@Value("${youtube.googleApiKey}")
	private String googleApiKey;// = "AIzaSyCN6boKAngTaSUXzbeTyeOTBALvw4tE0lI";
	
	@Value("${youtube.eduChanId}")
	private String youtubeEduChanId;// = "UC3yA8nDwraeOfnYfBWun83g";
	
	public SearchService() {
		
	}
	
	public SearchParameter getSearchField() {
		
		SearchParameter sp = getSearchOptionsFromYouTube(youtubeEduChanId,false);
		
		return sp;
	}
	
	public SearchParameter getSearchField(String selectedValue) { 
			
		SearchParameter searchParam = getSearchOptionsFromYouTube(selectedValue,true);
				
		return searchParam;
	}
	
	private SearchParameter getSearchOptionsFromYouTube(String channelId, boolean ignoreFirst) {
		SearchParameter sp = new SearchParameter();
		
		YouTube youtube = new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(), new HttpRequestInitializer() {
			
			
			public void initialize(HttpRequest arg0) throws IOException {
				// TODO Auto-generated method stub
				
			}
		}).setApplicationName("plg-youtube").build();;
		
		try {
			YouTube.Channels.List chanReq = youtube.channels().list("brandingSettings");
			chanReq.setKey(googleApiKey);
			chanReq.setId(channelId);
			
			ChannelListResponse chanRes = chanReq.execute();
			List<Channel>  chanList = chanRes.getItems();
			
			if(chanList != null){
				ChannelBrandingSettings settings =  chanList.get(0).getBrandingSettings();
				logger.info("============= YOUTUBE CHANNEL SETTINGS ==============");
				logger.info(settings.toString());
				
				sp.setFieldLabel(settings.getChannel().getFeaturedChannelsTitle());
				
				List<String> subChanIds = settings.getChannel().getFeaturedChannelsUrls();
			

				for(int i = 0; i < subChanIds.size(); i++){
					String id = subChanIds.get(i);
					String title = getYouTubeChannelTitleFromId(id);
					subChanIds.set(i, title + "=" + id );
				}
				
				if(ignoreFirst) {
					subChanIds.remove(0);
				}
				
				sp.setSearchParameters(subChanIds);
				
				logger.info("Sub Channels: " + sp.getSearchParameters().size());
				
				if(sp.getSearchParameters().size() > 1) {
					sp.setTerminalValue("N");
				}
				else {
					sp.setTerminalValue("Y");
				}
			}
			else {
				logger.info("No Such Channel Exists");
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return sp;
	}
	
	private String getYouTubeChannelTitleFromId(String channelId) {
		String title = "";
		YouTube youtube = new YouTube(new NetHttpTransport(), new JacksonFactory(), new HttpRequestInitializer() {
			
		
			public void initialize(HttpRequest arg0) throws IOException {
				// TODO Auto-generated method stub
				
			}
		});
		
		try {
			YouTube.Channels.List chanReq = youtube.channels().list("brandingSettings");
			chanReq.setKey(googleApiKey);
			chanReq.setId(channelId);
			
			ChannelListResponse chanRes = chanReq.execute();
			List<Channel>  chanList = chanRes.getItems();
			
			if(chanList != null){
	
				title = chanList.get(0).getBrandingSettings().getChannel().getTitle();
			}
			else {
				logger.info("No Such Channel Exists");
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return title;
	}
}	