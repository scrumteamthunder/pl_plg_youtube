package com.k12.services.bb.plugin.youtube; 

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.k12.services.bb.youtube.model.Account;

public interface AccountMapper {

	/******************************************
	 * Retrieves first student account located by User ID.
	 * 
	 * @return Account
	 */

	Account getStudentAccount(@Param("userID") String userID);

	/******************************************
	 * Retrieves first instructor located by User ID.
	 * 
	 * @return Account
	 */

	Account getInstructorByPersonKey(@Param("userID") String userID);
	
	
	List<Account> getStudentsInCourse(@Param("courseID") String courseID);
}