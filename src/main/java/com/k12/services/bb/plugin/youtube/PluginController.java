package com.k12.services.bb.plugin.youtube;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Elliott
 */

@Controller
@RequestMapping("/plugin")
public class PluginController {
	
	private static final Logger logger = LoggerFactory.getLogger(PluginController.class);

	@Autowired
	public ActivityService activityService;
	
	@Autowired
	public SearchService searchService;

	@Value("${youtube.googleApiKey}")
	String googleApiKey;
	
	@RequestMapping(value = "/getActivities", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	ActivityModelList getActivities(Locale locale, Model model,
			@RequestParam(value = "keyword", required = true) String keyword, String selections)
			throws Exception {
		selections = selections.replaceAll("\\[", "").replaceAll("\\]",""); //Scrubbing array param. 
		ArrayList<String> searchSelections = new ArrayList<String>(Arrays.asList(selections.split(",")));
		logger.info("What's Actually Getting Passed To /getActivities: " + "Keyword: " + keyword + " SearchSelections: " + searchSelections.toString());
		logger.info("Selections " + selections.toString());
		ActivityModelList list = activityService.getActivities(keyword, searchSelections);
		return list;
	}

	@RequestMapping(value = "/getSearchOptions", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	SearchParameter getSearchOptions(
			Locale locale,
			Model model,
			@RequestParam(value = "selectedValue", required = false) String selectedValue)
			throws Exception {
		SearchParameter s = new SearchParameter();

		if (selectedValue == null || selectedValue.length() == 0) {
			s = searchService.getSearchField();
		} else if (selectedValue.length() > 1) {
			s = searchService.getSearchField(selectedValue);
			logger.info("Value Being Passed to Search Service Is: " + selectedValue);
			
		}
		return s;
	}
	
	@RequestMapping(value = "/getYTVideoDetails/{videoId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	String getYTVideoDetails(
			@PathVariable String videoId,
			HttpServletRequest request, HttpServletResponse httpResponse,
			Locale locale, Model model)
			throws Exception {
		logger.info("Getting YouTube Video Details for video ID: " + videoId);
		String result = "";
		String urlString = "https://www.googleapis.com/youtube/v3/videos?part=contentDetails%2Csnippet&id=" + videoId + "&key=" + googleApiKey;
		
		URL url = new URL(urlString);
		BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
		String line;
		
		while((line = reader.readLine()) != null){
			result += line;
		}
		reader.close();
		
		return result;
	}
}
