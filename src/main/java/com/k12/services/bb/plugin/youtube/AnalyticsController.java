package com.k12.services.bb.plugin.youtube;

import java.security.SignatureException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.activemq.filter.function.replaceFunction;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Update.update;
import static org.springframework.data.mongodb.core.query.Query.query;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fueled.basecamp.model.base.LTIRequest;
import com.google.gson.Gson;
import com.k12.security.HmacSignature;
import com.k12.services.bb.youtube.model.Account;
import com.k12.services.bb.youtube.model.Attempt;
import com.k12.services.bb.youtube.model.StudentDataModel;

@Controller
@RequestMapping("/stats")
public class AnalyticsController {

	Gson gson = new Gson();

	@Autowired
	private MongoTemplate mongoTemplate;
	@Resource
	private AccountMapper accountMapper;

	private static final Logger logger = LoggerFactory
			.getLogger(AnalyticsController.class);

	private static final String validationKey = "H091gzKy8nPCcCknYQRg7CNZa6o=";

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/ca/{contentId}/{courseId}", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody
	ArrayList<StudentDataModel> getAnalytics(@PathVariable String contentId,
			@PathVariable String courseId, @RequestBody String json,
			HttpServletRequest request, HttpServletResponse httpResponse,
			Locale locale, Model model) throws Exception {

		String validationToken = "";
		Map<String, String> jsonMap = new HashMap<String, String>();
		jsonMap = (Map<String, String>) gson.fromJson(json, jsonMap.getClass());
		
		ObjectMapper mapper = new ObjectMapper();
		JSONObject jo = new JSONObject(json);

		LTIRequest ltiRequest = mapper.readValue(jo.get("ltiRequest").toString(), LTIRequest.class); 
		
		String userId = jsonMap.get("userId");
		String role =   jsonMap.get("role");
		String vToken = jsonMap.get("vToken");
		
		role = role.replace('"', ' ');
		role = role.trim();
		
		try {
			validationToken = HmacSignature.calculateRFC2104HMAC(userId + role,
					validationKey);
		} catch (SignatureException e) {
			model.addAttribute("errorTitle", "SignatureException Error");
			model.addAttribute("errorHeader", "SignatureException Error");
			model.addAttribute("errorMessage", "Failed to Sign Token");
			return new ArrayList<StudentDataModel>();
		}

		if (!vToken.trim().equals(validationToken.trim())) {
			return new ArrayList<StudentDataModel>();
		}

		logger.info("Content ID Is: " + contentId);

		Aggregation agg = newAggregation(
				match(where("contentID").is(contentId)),
				group("attemptID", "name", "userID").max("elapsedTime")
						.as("elapsedTime").max("duration").as("duration"));

		ArrayList<StudentDataModel> students = new ArrayList<StudentDataModel>();
		AggregationResults<Attempt> results = mongoTemplate.aggregate(agg,
				Attempt.class, Attempt.class);
		List<Attempt> attemptResults = results.getMappedResults();
		ArrayList<Attempt> attempts = new ArrayList<Attempt>();

		for (Attempt attempt : attemptResults) {
			attempts.add(attempt);
		}

		String pc = "";

		/*
		 * Look up the students who are enrolled in the course that we receive
		 * from the view.
		 */

		List<Account> accounts = accountMapper.getStudentsInCourse(courseId);
		List<Attempt> attemptsToBeRemoved = new ArrayList<Attempt>();

		for (Attempt attempt : attempts) {
			for (Attempt compare : attempts) {
				if (attempt.getUserID().equalsIgnoreCase(compare.getUserID())
						&& attempt.getElapsedTime() < compare.getElapsedTime()) {
					attemptsToBeRemoved.add(attempt);
				}
			}
		}

		for (Attempt attempt : attemptsToBeRemoved) {
			attempts.remove(attempt);
			logger.info("Attempts Size is: " + attempts.size());
		}

		for (Attempt attempt : attempts) {

			StudentDataModel s = new StudentDataModel();
			s.setName(attempt.getName());
			logger.info("Elapsed Time Is: " + attempt.getDuration());

			float percentComplete = (attempt.getElapsedTime() * 100)
					/ attempt.getDuration();

			/*
			 * If the student's watch time extends the duration of the video,
			 * give them a 100. Even though Nigel's client code will probably
			 * never let this happen to us.
			 */

			if (percentComplete > 100) {
				percentComplete = 100;
			}

			// Round our result to two decimal places.
			pc = String.format("%.0f", percentComplete);

			s.setPercentComplete(pc);

			/*
			 * TODO: This value is going to change whenever we implement some
			 * sort of system for scoring.
			 */

			s.setScore(0);

			students.add(s);
		}

		List<Account> resultList = new ArrayList<Account>();

		/*
		 * Here we figure out which account object(s) already have attempt data
		 * associated with them.
		 */

		for (Attempt attempt : attempts) {
			for (Account account : accounts) {
				if (account.getUserID().equalsIgnoreCase(attempt.getUserID()) || account.getBatchUID().equalsIgnoreCase(attempt.getUserID())) {
					resultList.add(account);
					continue;
				}
			}
		}

		/*
		 * This is where the magic happens - we take the result list we just
		 * generated and remove the accounts that already have usage data
		 */

		try {

			for (Account account : resultList) {
				accounts.remove(account);
			}

			/*
			 * We then give 0's to the scrubs who haven't watched the video.
			 */

			for (Account account : accounts) {
				StudentDataModel s = new StudentDataModel();
				s.setName(account.getFirst() + " " + account.getLast());
				s.setPercentComplete("0");
				s.setScore(0);
				students.add(s);
			}

		} catch (Exception e) {
			// Do Nothing.
		}

		if (!role.equals("INSTRUCTOR") && !role.equals("urn:lti:role:ims/lis/Instructor")) {
			for (StudentDataModel student : students) {
				student.setName("STUDENT A");
			}
		}

		return students;
	}

	/*
	 * This is where the attempt objects are queued and processed.
	 */

	@RequestMapping(value = "/push", method = RequestMethod.POST)
	@ResponseBody
	public void pushScoringData(@RequestBody String json) {
		Attempt attempt = gson.fromJson(json, Attempt.class);

		// We're only going to push data if we're not dealing with an instructor. 
		if (!attempt.getRole().equalsIgnoreCase("INSTRUCTOR")) {
			
			attempt.setContentID(attempt.getContentID().replaceAll("^\"|\"$",
					""));
			attempt.setName(attempt.getName().replaceAll("^\"|\"$", "").trim());

			Attempt existingAttempt = new Attempt();

			try {
				existingAttempt = mongoTemplate.findOne(new Query(Criteria
						.where("attemptID").is(attempt.getAttemptID())),
						Attempt.class);
			} catch (Exception e) {
				// Do nothing.
			}

			if (existingAttempt != null) {
				logger.info("Attempt with ID: "
						+ attempt.getAttemptID()
						+ " detected. Attempting to update record with new elapsed time.");

				if (attempt.getElapsedTime() <= existingAttempt
						.getElapsedTime()) {
					logger.debug("Incoming time is less than the existing time for this attempt, not updating record.");
				} else {

					mongoTemplate.updateFirst(
							query(where("attemptID").is(
									existingAttempt.getAttemptID())),
							update("elapsedTime", attempt.getElapsedTime()),
							Attempt.class);
				}

				mongoTemplate.updateFirst(
						query(where("attemptID").is(
								existingAttempt.getAttemptID())),
						update("duration", attempt.getDuration()),
						Attempt.class);

			} else {
				logger.info("No previous attempts detected, creating attempt with ID: "
						+ attempt.getAttemptID());
				mongoTemplate.insert(attempt);
			}

		}
	}
}