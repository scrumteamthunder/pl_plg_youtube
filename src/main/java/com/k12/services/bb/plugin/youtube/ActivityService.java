package com.k12.services.bb.plugin.youtube;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.PlaylistItem;
import com.google.api.services.youtube.model.PlaylistItemListResponse;
import com.google.api.services.youtube.model.PlaylistListResponse;
import com.google.api.services.youtube.model.ResourceId;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import com.google.api.services.youtube.model.Thumbnail;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;
import com.k12.services.bb.youtube.model.ActivityModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class ActivityService {
	
	private static final Logger logger = LoggerFactory.getLogger(ActivityService.class);
	

	@Value("${youtube.googleApiKey}") 
	private String googleApiKey;
	
	@Value("${youtube.serviceUrl}")
	private String youTubeServiceURL;

	
	public ActivityService()
	{
		
	}
	
	public ActivityModelList getActivities(String keyword, List<String> selections) throws Exception {

		logger.info("Inside gggetActivities.");

		String lastSelection = selections.get(selections.size() -1).toString();
		
		logger.info("LastSelection: " + lastSelection);
		
		YouTube youtube = new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(), new HttpRequestInitializer() {
			
			
			public void initialize(HttpRequest arg0) throws IOException {
				// TODO Auto-generated method stub
				
			}
		}).setApplicationName("plg-youtube").build();
		
		
		ActivityModelList activities = new ActivityModelList();
		ArrayList<ActivityModel> act = new ArrayList<ActivityModel>();
		
		logger.info("What's Actually Getting Passed " + "Keyword: " + keyword + " lastSelection: " + lastSelection);
		
		if(keyword.length() > 0){
			logger.info("========== USING YOUTUBE KEYWORD SEARCH ==========");
			 YouTube.Search.List search = youtube.search().list("id,snippet");
			 search.setKey(googleApiKey);
			 search.setQ(keyword);
			 search.setMaxResults((long) 50);
			 search.setSafeSearch("strict");
			 search.setFields("items(id/kind,id/videoId,snippet/title,snippet/description,snippet/thumbnails/high/url),nextPageToken");
			 
			 String nextToken = "";
			 Integer count = 0;
			 List<SearchResult> searchResultList = new ArrayList<SearchResult>();
			 
			 do {
				 search.setPageToken(nextToken);
				 SearchListResponse searchResponse = search.execute();
				 searchResultList.addAll(searchResponse.getItems());
				 
				 nextToken = searchResponse.getNextPageToken();
				 count++;
			 } while(nextToken != null && count < 4); // Stop after 200 results
			 
			 
			 logger.info("Number of results found: " + searchResultList.size());
			 
			 if(!searchResultList.iterator().hasNext()){
				 logger.info("No Results for search using keyword: " + keyword);
			 }
			 
			 for(SearchResult sr: searchResultList) {
				 ResourceId rId = sr.getId();
				 if (rId.getKind().equals("youtube#video")) {
		                Thumbnail thumbnail = sr.getSnippet().getThumbnails().getHigh();

//		                System.out.println(" Video Id" + rId.getVideoId());
//		                System.out.println(" Title: " + sr.getSnippet().getTitle());
//		                System.out.println(" Desc: " + sr.getSnippet().getDescription());
//		                System.out.println(" Thumbnail: " + thumbnail.getUrl());
//		                System.out.println("\n-------------------------------------------------------------\n");
		                
		                String youtubekey = UUID.randomUUID().toString();
		    			
		                ActivityModel am = new ActivityModel();
		    			LinkedHashMap<String, String> tableData = new LinkedHashMap<String, String>();
		    			
		    			String videoID = rId.getVideoId();
		    			String videoTitle = sr.getSnippet().getTitle();
		    			String videoDescription = sr.getSnippet().getDescription();
		    			String thumbnailURL = "<img src=\"" + thumbnail.getUrl() + "\" width=\"150\" height=\"110\">";
		    			
		    			tableData.put("Preview", thumbnailURL);
		    			tableData.put("Title", videoTitle);
		    			tableData.put("Description", videoDescription);
		    			tableData.put("Provider", "<img src=\"" + youTubeServiceURL + "/resources/images/youtubeicon.png\" width=\"44\" height=\"44\">");
		    			
		    			am.settableData(tableData);
		    			String previewURL = youTubeServiceURL + "/player/watch/" + videoID;
		    			
		    			am.setpreviewURL(previewURL);
		    			am.setaplusContentId(youtubekey);
		    			am.setCreateGradeColumn("N");
		    			am.setMaxPointValue(10);
		    			
		    			act.add(am); 
		            }
			 }
		}
		else {
			logger.info("========== USING YOUTUBE EDU SUBJECT AREA SEARCH ==========");
			YouTube.Playlists.List saSearch = youtube.playlists().list("snippet");
			saSearch.setKey(googleApiKey);
			saSearch.setChannelId(lastSelection);
			PlaylistListResponse saSearchResult = saSearch.execute();
			String plId = saSearchResult.getItems().get(0).getId();
			logger.info("Playlist ID: " + plId);
			
			if(plId != null) {
				YouTube.PlaylistItems.List plItemsSearch = youtube.playlistItems().list("id,contentDetails,snippet,status");
				plItemsSearch.setKey(googleApiKey);
				plItemsSearch.setMaxResults((long) 50);
				plItemsSearch.setPlaylistId(plId);
				//plItemsSearch.setFields("items(contentDetails/videoId,snippet/title,snippet/description,snippet/thumbnails/high/url,status/privacyStatus),nextPageToken");
				
				String nextToken = "";
				Integer count = 0;
				List<PlaylistItem> playlistItemList = new ArrayList<PlaylistItem>();
				
				do {
					plItemsSearch.setPageToken(nextToken);
                    PlaylistItemListResponse playlistItemResult = plItemsSearch.execute();

                    playlistItemList.addAll(playlistItemResult.getItems());

                    nextToken = playlistItemResult.getNextPageToken();
                    count++;
                } while (nextToken != null && count < 4);
				
				logger.info("Number of results found: " + playlistItemList.size());
				
				for(PlaylistItem plItem: playlistItemList){
					
					if(plItem.getStatus().getPrivacyStatus().equals("public")){
					
						Thumbnail thumbnail = plItem.getSnippet().getThumbnails().getHigh();
						
	//	                System.out.println(" Video Id" + plItem.getContentDetails().getVideoId());
	//	                System.out.println(" Title: " + plItem.getSnippet().getTitle());
	//	                System.out.println(" Desc: " + plItem.getSnippet().getDescription());
	//	                System.out.println(" Thumbnail: " + thumbnail.getUrl());
	//	                System.out.println("\n-------------------------------------------------------------\n");
						
						String youtubekey = UUID.randomUUID().toString();
		    			
		                ActivityModel am = new ActivityModel();
		    			LinkedHashMap<String, String> tableData = new LinkedHashMap<String, String>();
		    			
		    			String videoID = plItem.getContentDetails().getVideoId();
		    			String videoTitle = plItem.getSnippet().getTitle();
		    			String videoDescription = plItem.getSnippet().getDescription();
		    			String thumbnailURL = "<img src=\"" + thumbnail.getUrl() + "\" width=\"150\" height=\"110\">";
		    			
		    			tableData.put("Preview", thumbnailURL);
		    			tableData.put("Title", videoTitle);
		    			tableData.put("Description", videoDescription);
		    			tableData.put("Provider", "<img src=\"" + youTubeServiceURL + "/resources/images/youtubeicon.png\" width=\"44\" height=\"44\">");
		    			
		    			am.settableData(tableData);
		    			String previewURL = youTubeServiceURL + "/player/watch/" + videoID;
		    			
		    			am.setpreviewURL(previewURL);
		    			am.setaplusContentId(youtubekey);
		    			am.setCreateGradeColumn("N");
		    			am.setMaxPointValue(10);
		    			
		    			act.add(am); 
					}
				}
			}
			
		}
		
		
		activities.setActivities(act);

		return activities;
	}

	public boolean isLastSelectionSubCategory(Map<String, String> keys,
			String lastSelection) {
		boolean value = false;
		String potentialCategory = "";
		System.out.println("LAST SELECTION **" + lastSelection + " "
				+ "Keys Size Is: " + keys.size());
		String test = "Economics";
		System.out.println("Test String Size Is: " + test.length()
				+ " LastSelectionString Size Is " + lastSelection.length());
		try {
			potentialCategory = keys.get(lastSelection.substring(1)).toString();
			if (potentialCategory.length() > 1) {
				value = true;
			} else {
				value = false;
			}
		} catch (Exception e) {
			// do nothing
		}
		return value;
	}
}
