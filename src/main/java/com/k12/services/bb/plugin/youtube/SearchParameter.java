package com.k12.services.bb.plugin.youtube;

import java.util.List;

public class SearchParameter {

	private String terminalValue;
	private List<String> searchParameters;
	private String fieldLabel;


	public void setTerminalValue(String terminalValue) {
		this.terminalValue = terminalValue;
	}
	
	public String getTerminalValue() { 
		return terminalValue;
	}

	public String getTerminalValue(String terminalValue) {
		return terminalValue;
	}

	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}
	
	public String getFieldLabel() { 
		return fieldLabel;
	}

	public List<String> getSearchParameters() {
		return searchParameters;
	}

	public void setSearchParameters(List<String> searchParameters) {
		this.searchParameters = searchParameters;
	}
	

}