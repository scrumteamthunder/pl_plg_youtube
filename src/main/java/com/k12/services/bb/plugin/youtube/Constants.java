package com.k12.services.bb.plugin.youtube;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.util.HashMap;

import java.util.Map;



public class Constants {
	
	public static final String VTITLE = "Video Title";
	public static final String VDESC = "Description";
	public static final String VTHUMB = "Preview";
	private String orderPref; 
	
	public static final String TITLE = "<font color=1e1e1e> </font>";
	public static final String TITLE_DESCRIPTION = "Type a keyword <font color=red><i><b>or</b></i></font> select a category and subject area then press go to begin.";
	public static final String[] category = new String[]{"History & Social Sciences", "Languages", "Mathematics", "Science"};
	
	private static Map<String, String> keys;
	
	// For selection box HM values. 
	
	public static final String SUB_AREA_LABEL = "Subject"; 
	
	public static final String[] history_subcat = new String[] {"Economics", "Geography", "State & Local History"};
	
	public static final String[] language_subcat = new String[] {"Arabic", "Chinese", "French", "German", "Hindi", "Italian", 
		"Japanese", "Korean", "Polish", "Russian", "Spanish"};
	
	public static final String[] math_subcat = new String[] {"Algebra & Pre-Algebra", "Arithmetic", "Calculus & Pre-Calculus",
			"Geometry", "Modeling", "Statistics & Probability", "Trignometry"};
	
	public static final String[] science_subcat = new String[] {"Astronomy", "Biology & Life Science", "Chemistry", "Computer Science", 
		"Earth Science", "Engineering", "Physics"};
	
	
	// Category specific KV mapping. 



	
	private static final String CLIENTID = "PEAK12";
	private int maxResults  = 50; 
	private static final int TIMEOUT = 2000;
	private static boolean filter = true; 
	

	
	
	/* Regex for retrieving the video ID from a YouTube URL 
	 Interestingly enough, their API doesn't include this functionality for whatever reason.
	*/
	
	public static String getVideoID(String paramURL)
	{
		String videoID="";
		if (paramURL != null && paramURL.trim().length() > 0 && paramURL.startsWith("http"))
		{

			String tehexpression = "^.*((youtu.be"+ "\\/)" + "|(v\\/)|(\\/u\\/w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*"; 
			CharSequence input = paramURL;
			Pattern pattern = Pattern.compile(tehexpression,Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(input);
			if (matcher.matches())
			{
				String groupIndex1 = matcher.group(7);
				if(groupIndex1!=null && groupIndex1.length()==11){
					videoID = groupIndex1;
				}
			}
		}
		return videoID;
	}

	
	public static Map<String,String> getKeys()
	{

			if(keys == null){
			   keys = new HashMap<String, String>(); 
			
			
			//History Sub-Category 
			keys.put("Economics", "12");
			keys.put("Geography", "13");
			keys.put("State & Local History", "15");
			
			//Languages Sub-Category
			keys.put("Arabic", "25");
			keys.put("Chinese", "26");
			keys.put("French", "28");
			keys.put("German", "29");
			keys.put("Hindi", "31");
			keys.put("Italian", "32");
			keys.put("Japanese", "33");
			keys.put("Korean", "34");
			keys.put("Polish","35");
			keys.put("Russian", "37");
			keys.put("Spanish", "38");
			
			
			//Mathematics Sub-Category
			keys.put("Algebra & Pre-Algebra", "40");
			keys.put("Arithmetic", "41");
			keys.put("Calculus & Pre-Calculus", "42");
			keys.put("Geometry", "43");
			keys.put("Modeling", "44");
			keys.put("Statistics & Probability", "45");
			keys.put("Trignometry", "46");
			
			
			//Science Sub-Category 
			keys.put("Astronomy", "48");
			keys.put("Biology & Life Science", "49");
			keys.put("Chemistry", "50");
			keys.put("Computer Science", "51");
			keys.put("Earth Science", "52");
			keys.put("Engineering", "53");
			keys.put("Physics", "55");		
			}
			return keys;
	 
		}


	public void setOrderPref(String order)
	{
		if (order.length() == 0) 
		{
			this.orderPref.equals( "RELEVANCE");
		}
		else if (order.equals("View Count"))
		{
			this.orderPref = "VIEW_COUNT";
		} else if (order.equals("Relevance"))
		{
			this.orderPref = "RELEVANCE";
		} else if (order.equals("Rating"))
		{
			this.orderPref = "RATING";
		}		
	}
	
	public String getOrderPref()
	{
		return orderPref;
	}
	public String getClientID()
	{
		return CLIENTID; 
	}
	
	public int getTimeout()
	{
		return TIMEOUT; 
	}
	
	public boolean getFilter()
	{
		return filter; 
	}
	
	public void setMaxResults(int maxResults)
	{
		this.maxResults = maxResults; 
	}
	
	public int getMaxResults()
	{
		return maxResults; 
	}
}
	
	