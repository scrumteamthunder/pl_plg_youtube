package com.k12.services.bb.plugin.youtube;

import java.io.Serializable;
import java.util.List;
import com.k12.services.bb.youtube.model.ActivityModel;

public class ActivityModelList implements Serializable {
	static final long serialVersionUID = 1;

	private List<ActivityModel> activities;

	public ActivityModel getElement(int number) {
		return activities.get(number);
	}

	public List<ActivityModel> getActivities() {
		return activities;
	}

	public void setActivities(List<ActivityModel> activities) {
		this.activities = activities;
	}
}
