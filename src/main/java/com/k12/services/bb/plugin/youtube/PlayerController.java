package com.k12.services.bb.plugin.youtube;

import java.security.SignatureException;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fueled.basecamp.model.base.LTIRequest;
import com.k12.security.HmacSignature;
import com.k12.security.oauth.OAuthModel;
import com.k12.security.oauth.OAuthService;
import com.k12.services.bb.youtube.model.Account;
import com.k12.services.license.LicenseTools;
import com.mixpanel.mixpanelapi.MessageBuilder;
import com.mixpanel.mixpanelapi.MixpanelAPI;

@Controller public class PlayerController {

    @Value("${lti.serviceURL}") private    String ltiServiceURL;
    @Value("${lti.mixpanel_id}") private   String mixpanelID;
    @Value("${d2l.instance.host}") private String d2lInstance;

    @Resource private AccountMapper accountMapper;

    @Autowired private OAuthService oauthService;

    private static final Logger logger        = LoggerFactory.getLogger(PlayerController.class);
    private static final String validationKey = "H091gzKy8nPCcCknYQRg7CNZa6o=";

    @RequestMapping(value = "/player/watch") public String getVideoPlayerFromSG(
            @RequestParam(value = "custom_video_id") String videoId, HttpServletRequest request, Model model) {
        String consumerKey = oauthService.getConsumerKey((HttpServletRequest) request);
        String secret = new LicenseTools().getConsumerSecret(ltiServiceURL, consumerKey);

        OAuthModel m = oauthService.validateOAuth((HttpServletRequest) request, secret);
        if (m.getIsValid()) {
            request.setAttribute("pre-auth", true);
            return "forward:/player/watch/" + videoId;
        }
        else {
            model.addAttribute("errorTitle", "Authentication Error");
            model.addAttribute("errorHeader", "Authentication Error");
            model.addAttribute("errorMessage", "Access to this page has been denied due to an OAuth Validation error.");
            return "error";
        }
    }

    @RequestMapping(value = "/player/watch/{videoId}") public String getVideoPlayer(@PathVariable String videoId,
            HttpServletRequest request, HttpServletResponse response, Locale locale, Model model,
            @RequestParam Map<String, String> requestParameters,
            @RequestParam(value = "custom_content_context_id", required = false) String customContentContextID,
            @RequestParam(value = "context_id", required = false) String contextID,
            @RequestParam(value = "user_id", required = false) String en_userID,
            @RequestParam(value = "resource_link_id", required = false) String resourceLinkId,
            @RequestParam(value = "custom_external_person_key", required = false) String userID,
            @RequestParam(value = "roles", required = false) String roles,
            @RequestParam(value = "lis_person_name_full", required = false) String fullName,
            @RequestParam(value = "custom_external_course_key", required = false) String courseKey,
            @RequestParam(value = "providerid", required = false) String providerid) {

        if (request.getParameter("previewMode") != null) {
            return "forward:/playerPreview/watch/" + videoId;
        }

        logger.info("Context ID: " + request.getParameter("context_id"));

        String consumerKey = "";
        String validationToken = "";
        Boolean preAuthorized = request.getAttribute("pre-auth") == null ? false:true;

        LTIRequest ltiRequest = new LTIRequest(requestParameters);

        if (userID == null) {
            if (ltiRequest.getLtiToolConsumerCode() != null && ltiRequest.getLtiToolConsumerCode().equalsIgnoreCase("desire2learn")) {
                Map<String, String> custom = ltiRequest.getCustomProperties();
                userID = custom.get("ext_d2l_username");
            }
            else {
                userID = en_userID;
            }
        }

        try {
            validationToken = HmacSignature.calculateRFC2104HMAC(userID + roles, validationKey);
        }
        catch (SignatureException e) {
            model.addAttribute("errorTitle", "SignatureException Error");
            model.addAttribute("errorHeader", "SignatureException Error");
            model.addAttribute("errorMessage", "Failed to Sign Validation Token");
            return "error";
        }

        logger.info("inside post method of PlayerController: contextID is: " + contextID + " userID is: " + userID
                + " customContentContextID: " + customContentContextID + " contextID Is: " + contextID);

        if (!preAuthorized) {
            consumerKey = oauthService.getConsumerKey((HttpServletRequest) request);

            String secret = new LicenseTools().getConsumerSecret(ltiServiceURL, consumerKey);

            logger.info("consumer secret is: " + secret);
            OAuthModel m = oauthService.validateOAuth((HttpServletRequest) request, secret);
            if (!m.getIsValid()) {
                model.addAttribute("errorTitle", "Authentication Error");
                model.addAttribute("errorHeader", "Authentication Error");
                model.addAttribute("errorMessage", "Access to this page has been denied due to an OAuth Validation error.");
                return "error";
            }
        }

		/*
		 * Mixpanel Deployment Tracking.
		 */

        JSONObject props = new JSONObject();
        JSONObject previewEvent = new JSONObject();
        MixpanelAPI mixpanel = new MixpanelAPI();

        if (roles.equalsIgnoreCase("INSTRUCTOR")) {

            MessageBuilder messageBuilder = new MessageBuilder(mixpanelID);

            try {
                props.put("Product", "Open Educational Resources");
                props.put("From", "Search Results");
                props.put("Card Type", "Open Educational Resources");

                previewEvent = messageBuilder.event(userID, "Previewed", props);

                logger.debug("Sending Message To Mixpanel ID: " + mixpanelID + " Event: " + previewEvent.toString());

                mixpanel.sendMessage(previewEvent);

            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        model.addAttribute("videoId", videoId);
        model.addAttribute("contentId", resourceLinkId);
        model.addAttribute("providerid", providerid);
        model.addAttribute("roles", roles);
        model.addAttribute("d2lInstance", d2lInstance);

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            model.addAttribute("ltiRequest", objectMapper.writeValueAsString(ltiRequest));
        }
        catch (Exception e) {
            // Do nothing.
        }

        response.addCookie(new Cookie("lti.personKey", userID));
        response.addCookie(new Cookie("lti.contextID", contextID));
        response.addCookie(new Cookie("lti.user_id", en_userID));
        response.addCookie(new Cookie("lti.role", roles));
        response.addCookie(new Cookie("lti.oauth_consumer_key", consumerKey));
        response.addCookie(new Cookie("lti.customContentContextID", customContentContextID));
        response.addCookie(new Cookie("lti.validation_token", validationToken));
        response.addCookie(new Cookie("lti.resource_link_id", resourceLinkId));
        if (fullName != null) {
            response.addCookie(new Cookie("lti.full_name", fullName));
        }
        else {
            if (!roles.equals("INSTRUCTOR") && !roles.equals("urn:lti:role:ims/lis/Instructor")) {
                Account studentAccount = accountMapper.getStudentAccount(userID);
                fullName = studentAccount.getFirst() + " " + studentAccount.getLast();
            }
            else {
                Account instructorAccount = accountMapper.getInstructorByPersonKey(userID);
                fullName = instructorAccount.getFirst() + " " + instructorAccount.getLast();
            }
            response.addCookie(new Cookie("lti.full_name", fullName));
        }
        response.addCookie(new Cookie("lti.course_key", courseKey));

        return "watch";
    }

    @RequestMapping(value = "/playerPreview/watch/{videoId}") public String getVideoPlayerPreview(@PathVariable String videoId,
            HttpServletRequest request, HttpServletResponse response, Locale locale, Model model,
            @RequestParam Map<String, String> requestParameters,
            @RequestParam(value = "custom_content_context_id", required = false) String customContentContextID,
            @RequestParam(value = "context_id", required = false) String contextID,
            @RequestParam(value = "user_id", required = false) String en_userID,
            @RequestParam(value = "resource_link_id", required = false) String resourceLinkId,
            @RequestParam(value = "custom_external_person_key", required = false) String userID,
            @RequestParam(value = "roles", required = false) String roles,
            @RequestParam(value = "lis_person_name_full", required = false) String fullName,
            @RequestParam(value = "custom_external_course_key", required = false) String courseKey,
            @RequestParam(value = "providerid", required = false) String providerid) {

        model.addAttribute("videoId", videoId);
        model.addAttribute("contentId", "oer,5672b0c0-b9a5-4ae7-9eca-6bdd8e82c5a4");
        model.addAttribute("providerid", providerid);
        model.addAttribute("roles", roles);

        LTIRequest ltiRequest = new LTIRequest(requestParameters);

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            model.addAttribute("ltiRequest", objectMapper.writeValueAsString(ltiRequest));
        }
        catch (Exception e) {
            // Do nothing.
        }

        response.addCookie(new Cookie("lti.personKey", "demostudent1"));
        response.addCookie(new Cookie("lti.contextID", "1ca6eab5856ff129f9e5518a0af70288bc35380d70fa7fe9"));
        response.addCookie(new Cookie("lti.user_id", "dda77f501e24c952f5fe290375b016487febcdf10262cd44"));
        response.addCookie(new Cookie("lti.role", "INSTRUCTOR"));
        response.addCookie(new Cookie("lti.oauth_consumer_key", "TestCustomer1"));
        response.addCookie(new Cookie("lti.customContentContextID", ""));
        response.addCookie(new Cookie("lti.validation_token", "M+aC6MyqRrtWKiDWJPQuhXm7S6c="));
        response.addCookie(new Cookie("lti.resource_link_id", "oer,5672b0c0-b9a5-4ae7-9eca-6bdd8e82c5a4"));
        response.addCookie(new Cookie("lti.full_name", " Demo Student"));
        response.addCookie(new Cookie("lti.course_key", "Demo-01"));

        return "watch";
    }
}
