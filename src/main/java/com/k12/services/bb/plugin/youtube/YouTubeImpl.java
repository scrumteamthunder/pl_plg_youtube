package com.k12.services.bb.plugin.youtube;

import java.net.URL;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import com.k12.services.bb.youtube.model.YouTubeVideo;
import com.google.gdata.client.Query;
import com.google.gdata.client.youtube.YouTubeQuery;
import com.google.gdata.client.youtube.YouTubeService;
import com.google.gdata.data.Category;
import com.google.gdata.data.media.mediarss.MediaThumbnail;
import com.google.gdata.data.youtube.VideoEntry;
import com.google.gdata.data.youtube.VideoFeed;
import com.google.gdata.data.youtube.YouTubeMediaContent;
import com.google.gdata.data.youtube.YouTubeMediaGroup;
import com.k12.services.bb.youtube.model.YouTubeMedia;
import com.google.gdata.util.ServiceException;

public class YouTubeImpl {

	private static final String YOUTUBE_URL = "http://gdata.youtube.com/feeds/api/videos/";
	private static final String YOUTUBE_EDU_URL = "http://gdata.youtube.com/feeds/api/edu/lectures";
	private static final String YOUTUBE_EMBEDDED_URL = "http://www.youtube.com/v/";
	private static final String CAT_FINAL = "http://gdata.youtube.com/schemas/2007/categories.cat";

	private String clientID;
	private YouTubeService service;
	private VideoFeed videoFeed;
	private int timeout;
	public Query.CategoryFilter eduFilter = new Query.CategoryFilter();

	public YouTubeImpl(String clientID) {
		this.clientID = clientID;
		service = new YouTubeService(clientID);
		service.setConnectTimeout(timeout); // millis
	}

	public List<YouTubeVideo> getNextPage() throws IOException,
			ServiceException {
		if (videoFeed.getNextLink() != null) {
			videoFeed = service.getFeed(new URL(videoFeed.getNextLink()
					.getHref()), VideoFeed.class);

			List<VideoEntry> videos = videoFeed.getEntries();

			return convertVideos(videos);
		} else {
			return null;
		}
	}

	public List<YouTubeVideo> retrieveVideosPageByCategory(String keyword,
			boolean filter) throws IOException, ServiceException {

		YouTubeQuery query = new YouTubeQuery(new URL(YOUTUBE_EDU_URL));
		query.setStringCustomParameter("category", keyword);
		query.setOrderBy(YouTubeQuery.OrderBy.RELEVANCE);
		query.setSafeSearch(YouTubeQuery.SafeSearch.STRICT);

		query.setStringCustomParameter("lang", "en");
		query.setLanguageRestrict("en");

		videoFeed = service.query(query, VideoFeed.class);
		List<VideoEntry> videos = videoFeed.getEntries();

		return convertVideos(videos);
	}

	public List<YouTubeVideo> retrieveVideosPageByKeyword(String keyword,
			boolean filter) throws IOException, ServiceException {

		YouTubeQuery query = new YouTubeQuery(new URL(YOUTUBE_URL));

		Query.CategoryFilter categoryFilter = new Query.CategoryFilter();

		categoryFilter.addCategory(new Category(CAT_FINAL, "Education"));
		query.addCategoryFilter(categoryFilter);
		query.setOrderBy(YouTubeQuery.OrderBy.RELEVANCE);
		query.setFullTextQuery(keyword);
		query.setSafeSearch(YouTubeQuery.SafeSearch.STRICT);

		query.setLanguageRestrict("en");

		videoFeed = service.query(query, VideoFeed.class);
		List<VideoEntry> videos = videoFeed.getEntries();

		return convertVideos(videos);
	}

	public List<YouTubeVideo> retrieveVideos(String textQuery,
			String lastSelection, int maxResults, boolean filter, int timeout)
			throws Exception {

		service = new YouTubeService(clientID);
		service.setConnectTimeout(timeout); // millis
		YouTubeQuery query;
		// If keyword present, we can't search the EDU API so we'll have to rely
		// on category / keyword combination from Regular YouTube API
		if (textQuery.length() > 1) {
			query = new YouTubeQuery(new URL(YOUTUBE_URL));
			query.setFullTextQuery(textQuery);

		} else {
			query = new YouTubeQuery(new URL(YOUTUBE_EDU_URL));
		}

		query.setCountryRestriction("US");
		query.setStringCustomParameter("h1", "en");
		query.setStringCustomParameter("category", lastSelection);
		query.setOrderBy(YouTubeQuery.OrderBy.RELEVANCE);
		query.setSafeSearch(YouTubeQuery.SafeSearch.STRICT);
		query.setMaxResults(maxResults);
		videoFeed = service.query(query, VideoFeed.class);
		List<VideoEntry> videos = videoFeed.getEntries();

		return convertVideos(videos);

	}

	public List<YouTubeVideo> retrieveVideosByKeyword(String textQuery,
			int maxResults, boolean filter, int timeout) throws IOException,
			ServiceException {

		service = new YouTubeService(clientID);
		service.setConnectTimeout(timeout); // millis
		YouTubeQuery query = new YouTubeQuery(new URL(YOUTUBE_URL));

		Query.CategoryFilter categoryFilter = new Query.CategoryFilter();

		categoryFilter.addCategory(new Category(CAT_FINAL, "Education"));
		query.addCategoryFilter(categoryFilter);
		query.setOrderBy(YouTubeQuery.OrderBy.RELEVANCE);
		query.setFullTextQuery(textQuery);
		query.setSafeSearch(YouTubeQuery.SafeSearch.STRICT);
		query.setMaxResults(maxResults);
		query.setLanguageRestrict("en");

		videoFeed = service.query(query, VideoFeed.class);
		List<VideoEntry> videos = videoFeed.getEntries();

		return convertVideos(videos);

	}

	public List<YouTubeVideo> retrieveVideosByCategory(String category,
			int maxResults, boolean filter, int timeout) throws Exception {

		service = new YouTubeService(clientID);
		service.setConnectTimeout(timeout); // millis
		YouTubeQuery query = new YouTubeQuery(new URL(YOUTUBE_EDU_URL));
		query.setStringCustomParameter("category", category);
		query.setOrderBy(YouTubeQuery.OrderBy.RELEVANCE);
		query.setSafeSearch(YouTubeQuery.SafeSearch.STRICT);
		query.setMaxResults(maxResults);
		query.setStringCustomParameter("lang", "en");
		query.setLanguageRestrict("en");
		videoFeed = service.query(query, VideoFeed.class);
		List<VideoEntry> videos = videoFeed.getEntries();

		return convertVideos(videos);

	}

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public VideoFeed getVideoFeed() {
		return videoFeed;
	}

	public void setVideoFeed(VideoFeed videoFeed) {
		this.videoFeed = videoFeed;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	private List<YouTubeVideo> convertVideos(List<VideoEntry> videos) {

		List<YouTubeVideo> youtubeVideosList = new LinkedList<YouTubeVideo>();

		for (VideoEntry videoEntry : videos) {

			YouTubeVideo ytv = new YouTubeVideo();

			YouTubeMediaGroup mediaGroup = videoEntry.getMediaGroup();
			String webPlayerUrl = mediaGroup.getPlayer().getUrl();
			String videoTitle = mediaGroup.getTitle().getPlainTextContent();
			String videoDescription = mediaGroup.getDescription()
					.getPlainTextContent();

			ytv.setVideoTitle(videoTitle);
			ytv.setVideoDescription(videoDescription);
			ytv.setWebPlayerUrl(webPlayerUrl);
			ytv.setVideoTitle(videoTitle);

			String query = "?v=";
			int index = webPlayerUrl.indexOf(query);

			String embeddedWebPlayerUrl = webPlayerUrl.substring(index
					+ query.length());
			embeddedWebPlayerUrl = YOUTUBE_EMBEDDED_URL + embeddedWebPlayerUrl;
			ytv.setEmbeddedWebPlayerUrl(embeddedWebPlayerUrl);

			List<String> thumbnails = new LinkedList<String>();
			for (MediaThumbnail mediaThumbnail : mediaGroup.getThumbnails()) {
				thumbnails.add(mediaThumbnail.getUrl());
			}
			ytv.setThumbnails(thumbnails);

			List<YouTubeMedia> medias = new LinkedList<YouTubeMedia>();
			for (YouTubeMediaContent mediaContent : mediaGroup
					.getYouTubeContents()) {
				medias.add(new YouTubeMedia(mediaContent.getUrl(), mediaContent
						.getType()));
			}
			ytv.setMedias(medias);

			youtubeVideosList.add(ytv);

		}

		return youtubeVideosList;

	}

}
