<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/plg_youtube/resources/player/video/css/jPushMenu.css">
    <link rel="stylesheet" href="/plg_youtube/resources/player/video/css/bootstrap-switch.css">
    <link rel="stylesheet" href="/plg_youtube/resources/player/video/css/jquery.nouislider.css">
    <link rel="stylesheet" href="/plg_youtube/resources/player/video/css/bootstrap-tables.css">
    <link rel="stylesheet" href="/plg_youtube/resources/player/video/css/tablecloth.css">
    <link rel="stylesheet" href="/plg_youtube/resources/player/video/css/player.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,600,300,700,800'
          rel='stylesheet' type='text/css'>

    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="/plg_youtube/resources/player/video/js/modernizr.js"></script>
    <script src="/plg_youtube/resources/player/video/js/jPushMenu.js"></script>
    <script src="/plg_youtube/resources/player/video/js/bootstrap-switch.js"></script>
    <script src="/plg_youtube/resources/player/video/js/jquery.nouislider.js"></script>
    <script src="/plg_youtube/resources/player/video/js/jquery.cookie.js"></script>
    <script src="/plg_youtube/resources/player/video/js/jquery.k12ytplayer.js"></script>
    <script src="/plg_youtube/resources/player/video/js/utils.js"></script>
    <script src="/plg_youtube/resources/player/video/js/ChartNew.js"></script>
    <script src="/plg_youtube/resources/player/video/js/jquery.metadata.js"></script>
    <script src="/plg_youtube/resources/player/video/js/jquery.tablesorter.min.js"></script>
    <script src="/plg_youtube/resources/player/video/js/jquery.tablecloth.js"></script>


    <script>

        var videoid = '${videoId}';
        var contentId = '${contentId}';
        var videoPlayer = null;
        var ytcontrols = getQuerystring('ytcontrols');
        var youTubeURL = "http://www.youtube.com/watch?v=" + videoid;
        var youTubeMetaURL = "http://gdata.youtube.com/feeds/api/videos/" + videoid + "?v=2&alt=json";
        var videoConfig = {
            "techOrder": ["youtube"],
            "src": "http://www.youtube.com/watch?v=" + videoid + "&fs=1",
            "controls": false, "ytcontrols": false
        };

        var ltiRequest = ${ltiRequest};
        var d2lInstance = "${d2lInstance}";
    </script>

</head>

<body>

<div id="video-top">
    <img id="icon" src="/plg_youtube/resources/player/video/img/oericon.png"/>
    <p id="knanmessage">Viewer for KHAN ACADEMY<sup>&reg;</sup></p>
    <p id="k12ytp-title"></p>
    <!--         <button id="menuIcon" class="toggle-menu menu-right"></button> -->
</div>

<!-- Right menu element-->
<nav id="nav-wrapper"
     class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right">

    <div id="insideMenuButton">
        <button id="menuIcon" class="toggle-menu menu-right" tabindex=5></button>
    </div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
        <li class="active"><a href="#info" data-toggle="tab"><span
                class="glyphicon glyphicon-info-sign"></span></a></li>
        <li><a href="#analytics" id="analytics-link" data-toggle="tab"><span
                class="glyphicon glyphicon-signal"></span></a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="info">
            <div id="nav-content-wrapper">
                <div id="nav-content">
                    <div id="titleArea">
                        <p id="k12ytp-title" class="videoTitleInfo">My Video Title</p>
                    </div>
                    <div id="descArea">
                        <p id="k12ytp-description">My Video Description goes here.</p>
                    </div>
                    <div id="knannote">
                        <p>----------</p>
                        <p>NOTE: ALL KHAN ACADEMY<sup>&reg;</sup> content is available for free at <a id="khanlink"
                                                                                                      href="http://www.khanacademy.org"
                                                                                                      target="_blank">www.khanacademy.org</a>
                        </p>
                    </div>

                </div>
            </div>
        </div>
        <div class="tab-pane" id="analytics">
            <div id="nav-content-wrapper">
                <div id="nav-content">
                    <div id="titleArea" class="analyticsHeader">
                        <span id="numStudents">50</span> students were assigned to watch.
                    </div>
                    <div id="pieChartArea">
                        <canvas id="chartPieWatched" width="100" height="100"></canvas>
                        <div id="pieLabel">
                            <p>
                                <span class="color-square" style="background-color: #235281;"></span>
                                <span id="numWatched">40</span> Students Watched
                            </p>
                            <br>
                            <p>
                                <span class="color-square" style="background-color: #3d1652;"></span>
                                <span id="numNotWatched">10</span> Students Did Not Watch
                            </p>

                        </div>
                        <br>
                    </div>

                    <div id="barChartArea">
                        <canvas id="chartBarWatched" width="200" height="250"></canvas>
                    </div>

                    <div id="studentDataArea">

                        <table id="studentDataTable">
                            <thead>
                            <tr>
                                <th>Student</th>
                                <th>Watched</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>


<div id="player"></div>

<!--call jPushMenu, required-->
<script>
    var providerid = "${providerid}";
    var roles = "${roles}";
    if (providerid == "khan") {
        $("#knanmessage").show();
        if (roles.toLowerCase() == "instructor" || roles.toLowerCase() == "urn:lti:role:ims/lis/instructor")
            $("#knannote").show();
    }

    jQuery(document).ready(function ($) {

        //Chart Test Stuff


        var barOptions = {
            yAxisLabel: "Percent of Video Watched",
            xAxisLabel: "Number of Students",
            yAxisFontSize: 11,
            xAxisFontSize: 11,
            scaleShowGridLines: false
        };


        $('.toggle-menu').jPushMenu({
            closeOnClickInside: false,
            closeOnClickOutside: false
        });

        var inD2L = false;
        if (ltiRequest.ltiToolConsumerCode == "desire2learn") {
            inD2L = true;
        }

        $("#player").k12YTPlayer({
            videoId: videoid,
            metaDataUrl: "/plg_youtube/plugin/getYTVideoDetails/" + videoid,
            analyticsUrl: "/plugin_youtube/stats/push",
            analytics: true,
            inD2L: inD2L,
            D2Lwhoami: "${d2lInstance}/d2l/api/lp/1.5/users/whoami"
        });

        $.cookie.raw = true;
        vToken = $.cookie("lti.validation_token");
        vToken = vToken.replace(/"/g, '');

        var data = {
            userId: $.cookie("lti.personKey"),
            role: $.cookie("lti.role"),
            vToken: vToken,
            ltiRequest: ltiRequest
        };

        $.ajax({
            'async': false,
            'global': false,
            'type': 'POST',
            'data': JSON.stringify(data),
            'contentType': "application/json; charset=utf-8",
            'url': '/plugin_youtube/stats/ca/' + contentId + '/' + $.cookie("lti.course_key"),
            'success': function (data) {
                var totalStudents = data.length;
                var watched = 0;
                var didNotWatch = 0;

                var w10 = w20 = w30 = w40 = w50 = w60 = w70 = w80 = w90 = w100 = 0;

                $('#numStudents').text(totalStudents);

                $.each(data, function (index, student) {
                    if (student.percentComplete == 0) {
                        didNotWatch++;
                    } else if (student.percentComplete == 100) {
                        w100++;
                    } else if (student.percentComplete < 100 && student.percentComplete >= 90) {
                        w90++;
                    } else if (student.percentComplete < 90 && student.percentComplete >= 80) {
                        w80++;
                    } else if (student.percentComplete < 80 && student.percentComplete >= 70) {
                        w70++;
                    } else if (student.percentComplete < 70 && student.percentComplete >= 60) {
                        w60++;
                    } else if (student.percentComplete < 60 && student.percentComplete >= 50) {
                        w50++;
                    } else if (student.percentComplete < 50 && student.percentComplete >= 40) {
                        w40++;
                    } else if (student.percentComplete < 40 && student.percentComplete >= 30) {
                        w30++;
                    } else if (student.percentComplete < 30 && student.percentComplete >= 20) {
                        w20++;
                    } else {
                        w10++;
                    }

                    $('#studentDataTable > tbody:last').append("<tr><td>" + student.name + "</td><td>" + student.percentComplete + "%</td></tr>");

                });

                watched = totalStudents - didNotWatch;

                $('#numWatched').text(watched);
                $('#numNotWatched').text(didNotWatch);


                var barData = {
                    labels: ["10", "20", "30", "40", "50", "60", "70", "80", "90", "100"],
                    datasets: [
                        {
                            data: [w10, w20, w30, w40, w50, w60, w70, w80, w90, w100]
                        }
                    ]
                };

                var ctx = $("#chartBarWatched").get(0).getContext("2d");
                var barChart = new Chart(ctx).HorizontalBar(barData, barOptions);

                var pieData = [{value: watched, color: "#1e5283"}, {value: didNotWatch, color: "#3d1652"}];

                var ctx = $("#chartPieWatched").get(0).getContext("2d");
                var pieChart = new Chart(ctx).Pie(pieData);

                $("#studentDataTable").tablecloth({
                    theme: "default",
                    sortable: true,
                    striped: false,
                    customClass: "studentStats"
                });
            }
        });

        if ($.cookie("lti.role").toLowerCase().indexOf("instructor") == -1) {
            $("#studentDataTable").hide();
        }
    });
</script>

</body>

</html>
