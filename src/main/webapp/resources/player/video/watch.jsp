<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jPushMenu.css">
    <link rel="stylesheet" href="css/bootstrap-switch.css">
    <link rel="stylesheet" href="css/jquery.nouislider.css">
    <link rel="stylesheet" href="css/bootstrap-tables.css">
    <link rel="stylesheet" href="css/tablecloth.css">
    <link rel="stylesheet" href="css/player.css">

    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/jPushMenu.js"></script>
    <script src="js/bootstrap-switch.js"></script>
    <script src="js/jquery.nouislider.js"></script>
    <script src="js/jquery.k12ytplayer.js"></script>
    <script src="js/utils.js"></script>
    <script src="js/ChartNew.js"></script>
    <script src="js/jquery.metadata.js"></script>
    <script src="js/jquery.tablesorter.min.js"></script>
    <script src="js/jquery.tablecloth.js"></script>
    
    
    <script>

		var videoid = getQuerystring('videoid');
		var videoPlayer = null;
		var ytcontrols = getQuerystring('ytcontrols');	
		var youTubeURL = "http://www.youtube.com/watch?v=" + videoid;
		var youTubeMetaURL = "http://gdata.youtube.com/feeds/api/videos/" + videoid + "?v=2&alt=json";
		var videoConfig = { "techOrder": ["youtube"], 
							"src": "http://www.youtube.com/watch?v=" + videoid + "&fs=1", 
							"controls": false, "ytcontrols": false };
		
    </script>
    

</head>

<body>

 <div id="video-top" >
        <img id="icon" src="img/oericon.png" />
       	<p id="k12ytp-title"></p>
<!--         <button id="menuIcon" class="toggle-menu menu-right"></button> -->
    </div>

    <!-- Right menu element-->
    <nav id="nav-wrapper" class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right">
		
		<div id="insideMenuButton"><button id="menuIcon" class="toggle-menu menu-right"></button></div>
		
		<!-- Nav tabs -->
		<ul class="nav nav-tabs">
		  <li class="active"><a href="#info" data-toggle="tab"><span class="glyphicon glyphicon-info-sign"></span></a></li>
		  <li><a href="#analytics" id="analytics-link" data-toggle="tab"><span class="glyphicon glyphicon-signal"></span></a></li>
		</ul>
		
		<!-- Tab panes -->
		<div class="tab-content">
		  <div class="tab-pane active" id="info">
			  <div id="nav-content-wrapper">
				 	<div id="nav-content">
				 		<div id="titleArea"><p id="k12ytp-title">My Video Title</p></div>
				 		<div id="descArea"><p id="k12ytp-description">My Video Description goes here.</p></div>
				 	</div>
			 	</div>  
		  </div>
		  <div class="tab-pane" id="analytics">
		  	<div id="nav-content-wrapper">
				 	<div id="nav-content">
				 		<div id="titleArea"><span id="numStudents">50</span> students were assigned to watch.</div>
				 		<div id="barChartArea">
				 			<canvas id="chartBarWatched" width="200" height="250"></canvas>
				 		</div>
				 		<div id="pieChartArea">
				 			<canvas id="chartPieWatched" width="100" height="100"></canvas>
				 			<div id="pieLabel">
					 			<p><span class="color-square" style="background-color:#000000;"></span> <span id="numWatched">40</span> Students Watched</p>
					 			<p><span class="color-square" style="background-color:#999999;"></span> <span id="numNotWatched">10</span> Students Did Not Watch</p>
				 			</div>
				 		</div>
				 		<div id="studentDataArea">
				 		
				 			<table id="studentDataTable">
					            <thead>
					              <tr>
					                <th>Student</th>
					                <th>Watched</th>
					              </tr>
					            </thead>
					            <tbody>
					              
					            </tbody>
					          </table>
				
				 		</div>
				 	</div>
			 	</div>  
		  </div>
		</div>
    </nav>


 	
    <div id="player"></div>
  
   
    <!--call jPushMenu, required-->
    <script>
        jQuery(document).ready(function($) {
        	
        	//Chart Test Stuff
        	
                	
        	var barOptions = {
        			yAxisLabel: "Percent of Video Watched",
        			xAxisLabel: "Number of Students",
        			yAxisFontSize: 12,
        			xAxisFontSize: 12,
        			scaleShowGridLines: false
        	};
        	
        	
        	$('.toggle-menu').jPushMenu({
        		closeOnClickInside:false,
             	closeOnClickOutside:false
            });

           	$("#player").k12YTPlayer({
               	videoId: videoid,
               	metaDataUrl: "/plg_youtube/plugin/getYTVideoDetails/" + videoid,
               	analyticsUrl: "/plugin_youtube/stats/push"
            });
           	
           	$.ajax({
                'async': false,
                'global': false,
                'url': '/plg_youtube/stats/ca/123-abc',
                'success': function (data) {
                	var totalStudents = data.length;
                	var watched = 0;
                	var didNotWatch = 0;
                	
                	var w10=w20=w30=w40=w50=w60=w70=w80=w90=w100=0;
                	
                	$('#numStudents').text(totalStudents);
                	
                	$.each(data, function(index, student){
                		if(student.percentComplete == 0) {
                			didNotWatch++;
                		} else if (student.percentComplete == 100) {
                			w100++;
                		} else if (student.percentComplete < 100 && student.percentComplete >= 90) {
                			w90++;
                		} else if (student.percentComplete < 90 && student.percentComplete >= 80) {
                			w80++;
                		} else if (student.percentComplete < 80 && student.percentComplete >= 70) {
                			w70++;
                		} else if (student.percentComplete < 70 && student.percentComplete >= 60) {
                			w60++;
                		} else if (student.percentComplete < 60 && student.percentComplete >= 50) {
                			w50++;
                		} else if (student.percentComplete < 50 && student.percentComplete >= 40) {
                			w40++;
                		} else if (student.percentComplete < 40 && student.percentComplete >= 30) {
                			w30++;
                		} else if (student.percentComplete < 30 && student.percentComplete >= 20) {
                			w20++;
                		} else {
                			w10++;
                		}
                		
                		$('#studentDataTable > tbody:last').append("<tr><td>" + student.name + "</td><td>" + student.percentComplete +"%</td></tr>");
                		
                	});
                	
                	watched = totalStudents - didNotWatch;
                	
                	$('#numWatched').text(watched);
                	$('#numNotWatched').text(didNotWatch);
                	
                	
                	var barData = {
            				labels : ["10","20","30","40","50","60","70","80","90","100"],
            				datasets : [
            					{
            						data : [w10,w20,w30,w40,w50,w60,w70,w80,w90,w100]
            					}
            				]
            			};
                	
                	var ctx = $("#chartBarWatched").get(0).getContext("2d");
                	var barChart = new Chart(ctx).HorizontalBar(barData, barOptions);
                	
                	var pieData = [ {value: watched, color:"#000000"}, {value : didNotWatch, color : "#999999"} ];
                	
                	var ctx = $("#chartPieWatched").get(0).getContext("2d");
                	var pieChart = new Chart(ctx).Pie(pieData);
                	
                	$("#studentDataTable").tablecloth({ 
            			theme: "default",
            			sortable: true,
            			striped: false,
            			customClass: "studentStats"
            		});
                }
          	});
		});
    </script>

</body>

</html>