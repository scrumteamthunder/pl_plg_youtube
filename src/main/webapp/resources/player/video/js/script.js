$(window).load(function () {

    function getQuerystring(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/,
            "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex
                .exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g,
            " "));
    }
    var count = 0;
    //				var counter = setInterval(checkPlayer, 1000); // 1000 will
    // run it
    // every 1
    // second
    var videoTitle = "";
    var videoid = getQuerystring('videoid');
    console.log('video id is: ' + videoid);
    getVideoMeta(videoid);
    var userid = getQuerystring('uid');
    var courseid = getQuerystring('cid');
    var date = getQuerystring('date');
    var contextid = getQuerystring('contextid');
    var consumerkey = getQuerystring('ckey');
    var gradeColumnId = getQuerystring('gradeColumnId');
    var personkey = getQuerystring('personkey');
    getVideoMeta(videoid);

    function setTitle(videoTitle) {
        this.videoTitle = videoTitle;
    }

    function getVideoMeta(videoid) {
        var youTubeURL = "http://gdata.youtube.com/feeds/api/videos/" + videoid + "?v=2&alt=json";
        var json = (function () {
            var json = null;
            $
                .ajax({
                    'async': false,
                    'global': false,
                    'url': youTubeURL,
                    'dataType': "jsonp",
                    'success': function (data) {
                        json = data;
                        document.getElementById("title").innerHTML = json.entry.title.$t;
                        document.getElementById('description').innerHTML = json.entry.media$group.media$description.$t;
                    }
                });
            return json;
        })();

    }

    function storeInfo(info) {
        videoTitle = info.data.title;
        videoDescription = info.data.description;
        setTitle(videoTitle);
    }

    var durationView = function () {
        var startTime = null;
        var endTime = null;
        var viewDuration = 0;
        var percentOfView = 0;
        var playDuration = 0;
        var playPercent = 0;
        var maxPercentOfPlay = 0;
        var score = 0;
    }

    var loadInfo = function (videoid) {

        var dataUrl = "http://gdata.youtube.com/feeds/api/videos/" + videoid + "?v=2&alt=jsonc&callback=storeInfo";

        $.get(dataUrl, "", function (response) {
            eval(response); // This evals
            storeInfo(youtubeResponse);

        });

    };

    var ytPlayer = null;
    var startTime = null

    var currentTime = null;

    if (!window.onYouTubePlayerReady) {
        window.onYouTubePlayerReady = function (playerID) {
            ytPlayer = document.getElementById('video_' + playerID);
            ytPlayer.addEventListener('onStateChange',
                'eventListener_' + playerID);
            setInterval(checkPlayer, 1000);
        };
    }


    window.onbeforeunload = function () {
        sendScore();
        return "Leaving this page will submit your score.";
    };

    function sendScore() {
        checkPlayer();
        var score = document.getElementById("score")
        var json = '{' + '"userid" :' + '"' + userid + '",' + '"contextid" :' + '"' + contextid + '",' + '"name" :' + '"' + videoTitle + '",' + '"gradecolumnid" :' + '"' + gradeColumnId + '",' + '"consumerkey" :' + '"' + consumerkey + '",' + '"personkey" :' + '"' + personkey + '",' + '"courseid" :' + '"' + courseid + '",' + '"value" : ' + durationView.score.toFixed(1) + ',' + '"usageInMilliSeconds" : ' + count * 1000 + '}';

        $.ajax({
            url: '/lti_is/grade/',
            type: 'POST',
            contentType: 'application/json',
            data: json,
            dataType: 'json'
        });

    }

    function calculateScore() {
        var score = count / durationView.playDuration * 100;
        if (score > 100) {
            score = 100;
        }

        return score;

    }

    function checkPlayer() {
        if (startTime == null && ytPlayer.getPlayerState() > 0) {
            startTime = new Date();
            durationView.startTime = startTime;
            durationView.playDuration = ytPlayer.getDuration();
            durationView.maxPercentOfPlay = 0;
            currentTime = startTime;
        }
        if (ytPlayer.getPlayerState() > 0) {
            var percentOfPlay = 100 * (ytPlayer
                .getCurrentTime() / ytPlayer.getDuration());
            if (ytPlayer.getPlayerState() == 1) {
                count = count + 1;
                currentTime = new Date();
            }
            if (ytPlayer.getPlayerState() > 1) {
                var ncTime = new Date();
                var diffTime = ncTime - currentTime;
                startTime = new Date(startTime.getTime() + diffTime);
                currentTime = ncTime;

            }
            durationView.viewDuration = (currentTime - startTime) / 1000;
            durationView.percentOfView = 100 * (durationView.viewDuration / (1000 * ytPlayer
                .getDuration()));
            var currentPlayPercent = 100 * (ytPlayer
                .getCurrentTime() / ytPlayer.getDuration());
            durationView.percentOfPlay = currentPlayPercent;
            durationView.maxPercentOfPlay = (currentPlayPercent > durationView.maxPercentOfPlay) ? currentPlayPercent : durationView.maxPercentOfPlay;
            durationView.score = calculateScore();

            var json = '{' + '"userid" :' + '"' + userid + '",' + '"contextid" :' + '"' + contextid + '",' + '"name" :' + '"' + videoTitle + '",' + '"gradecolumnid" :' + '"' + gradeColumnId + '",' + '"value" : ' + durationView.score.toFixed(1) + '}';



        }

    }

    var videoTitle = "";
    var videoDescription = "";
    loadInfo(videoid);

    $('#player').youTubeEmbed(
        "http://www.youtube.com/watch?v=" + videoid);

    var uagent = navigator.userAgent.toLowerCase();

    if (uagent.search("iphone") > -1 || uagent.search("ipod") > -1 || uagent.search("ipad") > -1 || uagent.search("appletv") > -1 || uagent.search("mobileChrome") > -1) {

        window.location = "http://m.youtube.com/watch?client=mv-google&hl=en&gl=MY&v=" + videoid;
    } else {
        // Do nothing
    }

    $('form').submit(function () {
        $('#player').youTubeEmbed($('#url').val());
        $('#url').val('');
        return false;
    });

});