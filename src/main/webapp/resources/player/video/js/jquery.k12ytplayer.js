/*
 *  K12 jQuery YouTube player
 *
 *  Made by Nigel Bazzeghin
 */

// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.

;
var ytplayer, pusher, languageList;
var playStateTime = 0;
var playingState = false;
var controlsHidden = false;
var ytVideoId = null;
var first = true;
var postData = false;
pusher = setInterval(function(){
	if(playingState) {
		playStateTime++;
		//console.log(playStateTime);
		if(playStateTime % 5 == 0){
			$(".k12YTPlayer").data("plugin_k12YTPlayer").postAnalyticsData();
		}
	}
}, 1000);
function onYouTubeIframeAPIReady(){
	ytplayer = new YT.Player('ytplayer',{
		events: {
			onReady: 'onPlayerReady',
			onStateChange: 'onPlayerStateChange'
		}
	});
}
function onPlayerReady(event){
	setInterval(function(){
		var complete_perc = (ytplayer.getCurrentTime() / ytplayer.getDuration()) * 100;
		var currentTime = ytplayer.getCurrentTime();
		$(".k12ytp-scrubber").val(complete_perc);
		$('#current-time').html(SecondsToHMS(currentTime));
		$('#duration').html(SecondsToHMS(ytplayer.getDuration() - 1));
		
		var $captionLines = $( "#trans-content" ).children();
		$captionLines.each(function(){
			if(currentTime >= $(this).data("time") && currentTime <= $(this).data("time") + $(this).data("dur") + 0.1) {
				$(this).addClass("caption-highlight");
			} else {
				$(this).removeClass("caption-highlight");
			}
		});
	}, 1000);
	
}
function onPlayerStateChange(event) {
	if(first){
		ytplayer.loadModule("captions");
		setTimeout(function(){
			ytplayer.loadModule("captions");
			setTimeout(function(){
				languageList = getLanguages();
				if(languageList.length > 0){
					$.each(languageList,function(index,lang){
						$('#trans-lang').append(new Option(lang.name, lang.code));
					});
					$("#trans-lang").val("en");
					localStorage.setItem("k12ytplang", "en");
					$(".cc-icon").fadeIn();
					$(".trans-icon").fadeIn();
					 $.ajax({
				            type: "GET",
				            url: window.location.protocol + "//video.google.com/timedtext?lang=en&v=" + ytVideoId,
				            cache: false,
				            dataType: "xml",
				            success: function(xml) {
				                $(xml).find('text').each(function(){
				                    var text = $(this).text();
				                    var time = $(this).attr('start');
				                    var dur = $(this).attr('dur');
				                    var date = new Date(null);
				                    date.setSeconds(time);
				                    var niceTime = date.toISOString().substr(14, 5);
				                    if(time >= 3600){
				                    	 niceTime = date.toISOString().substr(11, 8);
				                    }
				                    $("#trans-content").append('<div class="caption-line" data-time="'+time+'" data-dur="'+dur+'"><div class="caption-time">'+niceTime+'</div><div class="caption-text">'+text+'</div></div>');
				                });
				            }
				        });
				}
			},1500);
		},1500);
		first = false;
	}
	if (event.data == YT.PlayerState.PLAYING){
		playingState = true;
		$("#btn-play span").addClass('glyphicon-pause');
		if(controlsHidden) {
			controlsHidden = false;
			$('#k12ytp-controls').show();
		}
	}
	else {
		$("#btn-play span").removeClass('glyphicon-pause');
		playingState = false;
		$(".k12YTPlayer").data("plugin_k12YTPlayer").postAnalyticsData();
	}
}
function SecondsToHMS(d){
	d = Number(d);
	var h = Math.floor(d / 3600);
	var m = Math.floor(d % 3600 / 60);
	var s = Math.floor(d % 3600 % 60);
	return ((h > 0 ? (h >= 10 ? h : '0' + h) + ':' : '') +  (m > 0 ? m: '0') + ':' + (s > 0 ? (s >= 10 ? s : '0' + s): '00')  );
}

function generateUUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x7|0x8)).toString(16);
    });
    return uuid;
}

function getLanguages() {
	var langs = [];
	$.each(ytplayer.getOption("captions","tracklist"),function(){
		langs.push({code:this.languageCode,name:this.languageName});
	});
	return langs;
}

(function ( $, window, document, undefined ) {

		// undefined is used here as the undefined global variable in ECMAScript 3 is
		// mutable (ie. it can be changed by someone else). undefined isn't really being
		// passed in so we can ensure the value of it is truly undefined. In ES5, undefined
		// can no longer be modified.

		// window and document are passed through as local variable rather than global
		// as this (slightly) quickens the resolution process and can be more efficiently
		// minified (especially when both are regularly referenced in your plugin).

		// Create the defaults once
		var pluginName = "k12YTPlayer",
				defaults = {
					videoId: "M7lc1UVf-VE",
					metaDataUrl: "https://www.googleapis.com/youtube/v3/videos?part=contentDetails%2Csnippet&id=",
					title: "Default Title",
					description: "This is the default description",
					duration: "0:00",
					analytics: false,
					postData:false,
					analyticsUrl: "/plg_youtube/stats/push",
					attemptUUID: generateUUID()
			};

		// The actual plugin constructor
		function Plugin ( element, options ) {
				this.element = element;
				// jQuery has an extend method which merges the contents of two or
				// more objects, storing the result in the first object. The first object
				// is generally empty as we don't want to alter the default options for
				// future instances of the plugin
				$(this.element).addClass(pluginName);
				this.settings = $.extend( {}, defaults, options );
				this._defaults = defaults;
				this._name = pluginName;
				this.init();
		}
		
		Plugin.prototype = {
				init: function () {
					
					// Place initialization logic here
					// You already have access to the DOM element and
					// the options via the instance, e.g. this.element
					// and this.settings
					// you can add more functions like the one below and
					// call them like so: this.yourOtherFunction(this.element, this.settings).
					
					var tag = document.createElement('script');
					tag.src = "//www.youtube.com/iframe_api";
					var firstScriptTag = document.getElementsByTagName('script')[0];
					firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
					
					this.buildPoster(this);
					$(this.element).append(this.getPlayerIframe());
					this.buildControlBar(this);
					
					//$(this.element).append(this.getControlBar());
					this.getMetaData(this);
						
						
				},
				buildPoster: function(plg){
					var poster = "<div id='k12ytp-poster'>" + 
					                 '<div id="k12ytp-poster-playbutton"><span class="glyphicon glyphicon-play"></div>'+
								 "</div>";
					
					if (Modernizr.touch) {   
		        	    //TODO: Check for alternative poster options for mobile devices  
		        	} else {   
		        		$(plg.element).append(poster);
		        		plg.posterShown = true;
						
						$("#k12ytp-poster").css('background-image','url(https://i1.ytimg.com/vi/' + this.settings.videoId + '/0.jpg)');
						
						$("#k12ytp-poster").on('click', function(){
							$("#k12ytp-poster").hide();
							plg.posterShown = false;
							$("#btn-play span").toggleClass('glyphicon-pause');
							ytplayer.playVideo();
						});
		        	}  
				},
				
				getPlayerIframe:function() {
					ytVideoId = this.settings.videoId;
					return '<iframe id="ytplayer" type="text/html" src="https://www.youtube.com/embed/' + this.settings.videoId + '?controls=0&wmode=transparent&rel=0&showinfo=0&enablejsapi=1" frameborder="0" allowfullscreen></iframe>';
				},
				buildControlBar: function (plg) {
					
					var bar =  '<div id="k12ytp-controls">'
						    	+'<div class="k12ytp-scrubber"></div>'
						    	+'<div id="k12ytp-buttons">'
						    	+'	<div id="k12ytp-button">'
						    	+'		<button id="btn-play" type="button" class="btn btn-default" aria-label="Play/Pause" tabindex=1><span class="glyphicon glyphicon-play"></button>'
						    	+'	</div>'
						    	+'	<div id="k12ytp-button">'
						    	+'		<button id="btn-volume" type="button" class="btn btn-default" aria-label="Volume Slider" tabindex=2><span class="glyphicon glyphicon-volume-up"></button>'
						    	+'	</div>'
						    	+'	<div id="k12ytp-button">'
						    	+'		<div id="volume-slider"></div>'
						    	+'	</div>'
						    	+'	<div id="k12ytp-button">'
						    	+'		<div id="time">'
						    	+'			<span id="current-time">0:00</span> / <span id="duration">0:00</span>'
						    	+'		</div>'
						    	+'	</div>'
						    	+'	<div id="k12ytp-button" class="pull-right">'
						    	+'		<button id="btn-options" type="button" class="btn btn-default" tabindex=4 aria-label="Settings" role="menu"><span class="glyphicon glyphicon-cog"></button>'
						    	+'	</div>'
						    	+'	<div id="k12ytp-button" class="trans-icon pull-right">'
						    	+'		<button id="btn-trans" type="button" class="btn btn-default" tabindex=3 aria-label="Transcripts" ><span class="fa fa-file-text-o"></button>'
						    	+'	</div>'
						    	+'	<div id="k12ytp-button" class="cc-icon pull-right">'
						    	+'		<button id="btn-cc" type="button" class="btn btn-default" tabindex=3 aria-label="Closed Captions" ><span class="fa fa-cc"></button>'
						    	+'	</div>'
						    	+'	</div>'
						    	+'	</div>';		
					$(this.element).append(bar);
					
					var transcriptPannel = '<div id="transcript-pannel" >'
											+'<div id="trans-return"><span class="fa fa-angle-left fa-3x" style="vertical-align: middle;"/> Return to video</div>'
											+'<div id="trans-control" class="form-group"><label for="trans-lang">Select Transcript Language</label><select id="trans-lang" class="form-control" tabindex=5></select></div>'
											+'<div id="trans-content">'
											+'</div>'
											+'</div>';	
					
					$(this.element).append(transcriptPannel);
					
					if (Modernizr.touch) {  
						$('#k12ytp-controls').hide();
						controlsHidden = true;
					}
					
					$('#volume-slider').hide();
					
					$("#btn-play").on('click', function(){
	
						if(plg.posterShown){
							$("#k12ytp-poster").hide();
							plg.posterShown = false;
							$("#btn-play span").toggleClass('glyphicon-pause');
							ytplayer.playVideo();
						}
						else {
							if($("#btn-play span").hasClass('glyphicon-pause')){
								$("#btn-play span").toggleClass('glyphicon-pause');
								ytplayer.pauseVideo();
							}
							else {
								$("#btn-play span").toggleClass('glyphicon-pause');
								ytplayer.playVideo();
							}
						}
						
					});
					
					$('#btn-volume').on('click', function(){
//						if($("#btn-volume").hasClass('glyphicon-volume-up')){
//							plg.muteVol = $('#volume-slider').val();
//							$('#volume-slider').val(0);
//							ytplayer.setVolume(0);
//						}
//						else {
//							$('#volume-slider').val(plg.muteVol);
//							ytplayer.setVolume(plg.muteVol);
//						}
//						$('#btn-volume').toggleClass('glyphicon-volume-off');
//						$('#btn-volume').toggleClass('glyphicon-volume-up');
						$('#volume-slider').toggle();
						
					});
					
					$('#btn-cc').on('click', function(){
						$(".cc-icon").toggleClass('cc-on');
						if($(".cc-icon").hasClass('cc-on')){
							ytplayer.setOption("captions", "track", {"languageCode": "en"});
						}
						else {
							ytplayer.setOption("captions", "track", {});
						}
					});
					
					$('#btn-trans').on('click', function(){
						$("body").toggleClass("transcripts-open");
					});
					
					$("#trans-return").click(function(){
						$("body").removeClass("transcripts-open");
					});
					
					$("#trans-content").on("click",".caption-line",function(){
						ytplayer.seekTo($(this).data("time"));
					});
					
					$('#btn-options').popover({
						trigger: 'click',
						placement: 'top',
						html: true,
						content: '<select id="dd-quality" class="form-control" tabindex=4>'
								+'<option>Auto</option>'
								+'</select>'
								+'<select id="dd-lang" class="form-control" tabindex=5>'
								+'<option value="">Language</option>'
								+'</select>'
					});
					
					
					
					$('#btn-options').on('click', function(){
						var buttonOffset = $('#btn-options').offset();
						$('.popover').css('left',buttonOffset.left - $('.popover').width() + 24 + 'px');
						//$('#tgl-annotations').bootstrapSwitch();
						$('.bootstrap-switch').on('click', function(){
							
						});
						var levels = ytplayer.getAvailableQualityLevels();
						$.each(levels, function(index, value){
							if(value == 'hd1080') {
								$('#dd-quality').append(new Option('1080p', value));
							}
							else if (value == 'hd720') {
								$('#dd-quality').append(new Option('720p',value));
							}
							else if (value == 'large') {
								$('#dd-quality').append(new Option('480p',value));
							}
							else if (value == 'medium') {
								$('#dd-quality').append(new Option('360p',value));
							}
							else if (value == 'small') {
								$('#dd-quality').append(new Option('240p',value));
							}
							else if (value == 'tiny') {
								$('#dd-quality').append(new Option('144p', value));
							}
						});	
						if(languageList.length > 0){
							$("#dd-lang").show();
							$.each(languageList,function(index,lang){
								$('#dd-lang').append(new Option(lang.name, lang.code));
							});
							$('#dd-lang').val(localStorage.getItem("k12ytplang"));
						}
					});
					
					$(document).on('change','#dd-quality', function(){
						ytplayer.setPlaybackQuality($(this).val());
					});
					
					$(document).on('change','#dd-lang', function(){
						plg.setLanguage($(this).val());
					});
					
					$(document).on('change','#trans-lang', function(){
						plg.setLanguage($(this).val());
					});
					
					$(".k12ytp-scrubber").noUiSlider({
						range: [0, 100],
						start: 0,
						step: 1,
						handles: 1,
						slide: function(){
							if(plg.posterShown){
								$("#k12ytp-poster").hide();
								plg.posterShown = false;
							}
							var slidePercent = $(".k12ytp-scrubber").val() / 100;
							var videoDuraction = ytplayer.getDuration();
							ytplayer.seekTo(videoDuraction * slidePercent);
						}
					});
					
					$("#volume-slider").noUiSlider({
						range: [0, 100],
						start: 100,
						step: 1,
						handles: 1,
						slide : function() {
							if($('#btn-volume').hasClass('glyphicon-volume-off')) {
								$('#btn-volume').toggleClass('glyphicon-volume-off');
								$('#btn-volume').toggleClass('glyphicon-volume-up');
							}
							ytplayer.setVolume($("#volume-slider").val());
						}
					});
					
				},
				getMetaData: function(plg) {
                	$.ajax({
	                  'async': false,
	                  'global': false,
	                  'url': this.settings.metaDataUrl,
	                  'success': function (data) {
	                	  plg.settings.title = data.items[0].snippet.title;
	                	  plg.settings.description = data.items[0].snippet.description;
	                	  plg.setTitleDesc();
	                  }
                	});
				},
				setLanguage: function(lang) {
					localStorage.setItem("k12ytplang", lang);
					$("#trans-lang").val(lang);
					$('#dd-lang').val(lang);
					if(!$(".cc-icon").hasClass('cc-on')){
						$(".cc-icon").addClass('cc-on');
					}
					$("#trans-content").empty();
					$.ajax({
			            type: "GET",
			            url: window.location.protocol + "//video.google.com/timedtext?lang="+lang+"&v=" + ytVideoId,
			            cache: false,
			            dataType: "xml",
			            success: function(xml) {
			                $(xml).find('text').each(function(){
			                    var text = $(this).text();
			                    var time = $(this).attr('start');
			                    var dur = $(this).attr('dur');
			                    var date = new Date(null);
			                    date.setSeconds(time);
			                    var niceTime = date.toISOString().substr(14, 5);
			                    if(time >= 3600){
			                    	 niceTime = date.toISOString().substr(11, 8);
			                    }
			                    $("#trans-content").append('<div class="caption-line" data-time="'+time+'" data-dur="'+dur+'"><div class="caption-time">'+niceTime+'</div><div class="caption-text">'+text+'</div></div>');
			                });
			            }
			        });
					ytplayer.setOption("captions", "track", {"languageCode": lang});
				},
				setTitleDesc: function() {
					var title = this.settings.title;
					var description = this.settings.description;
					
					document.title = title;
					
					$('p#k12ytp-title').each(function(){
              		   $(this).html(title);
              	   });
              	   $('p#k12ytp-description').each(function(){
              		   $(this).html(description);
              	   });
				},
				postAnalyticsData: function() {
					
					if(this.settings.analytics ==  true) {
						//console.log("playStateTime: "  + playStateTime + " | currentTime(): " + ytplayer.getCurrentTime());
						var et = 0;
						if(ytplayer.getCurrentTime() > playStateTime) {
							et = ytplayer.getCurrentTime() * (playStateTime / ytplayer.getCurrentTime());
						}
						else {
							et = ytplayer.getCurrentTime();
						}
						
						
						if(this.settings.inD2L ==  true) {
							if(!postData){
								$.ajax({
			                        url: this.settings.D2Lwhoami,
			                        dataType:"jsonp",
			                        type: 'GET',
									success: function(data){
										$.cookie("lti.personKey",data.Identifier);
										postData = true;
									},
			                        contentType: "application/json; charset=utf-8",
			                    });
							}	
						}
						else {
							postData = true;
						}
						var data = {
								"attemptID" : this.settings.attemptUUID, 
							    "userID" : $.cookie("lti.personKey"), 
							    "contentID" : $.cookie("lti.resource_link_id"),
							    "videoID" : this.settings.videoId, 
							    "courseID" : $.cookie("lti.course_key"),
							    "name" : $.cookie("lti.full_name"),
							    "duration" :  Math.round(ytplayer.getDuration()), 
							    "elapsedTime" : et, 
							    "role" : $.cookie("lti.role") 
						};
						
						if(postData){
							$.ajax({
		                        url: this.settings.analyticsUrl,
		                        data: JSON.stringify(data),
		                        type: 'POST',
		                        contentType: "application/json; charset=utf-8",
		                    });
						}
					}
				}
		};

		// A really lightweight plugin wrapper around the constructor,
		// preventing against multiple instantiations
		$.fn[ pluginName ] = function ( options ) {
				this.each(function() {
						if ( !$.data( this, "plugin_" + pluginName ) ) {
								$.data( this, "plugin_" + pluginName, new Plugin( this, options ) );
						}
				});

				// chain jQuery functions
				return this;
		};

})( jQuery, window, document );